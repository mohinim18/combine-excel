const Excel = require('exceljs');
const path = require('path');


/***
 * {"filelist":[
 *              {"filepath":"","sheet_name":"Data","header-bold":true/false},
 *              {"filepath":"","sheet_name":"Charts","header-bold":true/false}
 *             ],
 *  op_file:""
 * }
 */
const csvtoexcel=async({targetWorkbook,fileObj,op_file})=>{
    console.log("Sheet Name : "+fileObj.sheet_name)
    let targetWorksheet = targetWorkbook.addWorksheet(fileObj.sheet_name);   
    let sourceWorkbook = new Excel.Workbook();
    console.log("Filepath : "+fileObj.filepath);
    sourceWorksheet = await sourceWorkbook.csv.readFile(fileObj.filepath);
   //  let sourceWorksheet = sourceWorkbook.worksheets;
   //  console.log(sourceWorksheet);
    sourceWorksheet.eachRow({ includeEmpty: false }, (row, rowNumber) => {
       var targetRow = targetWorksheet.getRow(rowNumber);
       targetRow.font=(rowNumber==1 && fileObj["header-bold"])?{bold: true}:{};
       row.eachCell({ includeEmpty: false }, (cell, cellNumber) => {
           targetRow.getCell(cellNumber).value = cell.value;
       });
       row.commit();
   });
   await targetWorkbook.xlsx.writeFile(op_file);
}

const xls2xlsx=async({targetWorkbook,fileObj,op_file})=>{
    console.log("Sheet Name : "+fileObj.sheet_name)
    let targetWorksheet = targetWorkbook.addWorksheet(fileObj.sheet_name);   
    let sourceWorkbook = new Excel.Workbook();
    console.log("Filepath : "+fileObj.filepath);
    sourceWorkbook = await sourceWorkbook.xlsx.readFile(fileObj.filepath);
    let sourceWorksheet = sourceWorkbook.worksheets[0]
    //clone using model
    // targetWorksheet.model=JSON.parse(JSON.stringify(sourceWorkbook.model));
    sourceWorksheet.eachRow({ includeEmpty: false }, (row, rowNumber) => {
       var targetRow = targetWorksheet.getRow(rowNumber);
       targetRow.font=(rowNumber==1 && fileObj["header-bold"])?{bold: true}:{};
       row.eachCell({ includeEmpty: false }, (cell, cellNumber) => {
           targetRow.getCell(cellNumber).value = cell.value;
       });
       row.commit();
   });
   await targetWorkbook.xlsx.writeFile(op_file);
}

module.exports=async(Obj)=>{
    if(!Obj.filelist || Obj.filelist.length==0)
        throw new Error('Input files not present');
    
    if(!Obj.op_file)
        throw new Error('Output file not present');

    // if(path.extname(Obj.op_file)!=".xlsx")
    //     throw new Error('Invalid extension');

    let targetWorkbook= new Excel.Workbook();
    await targetWorkbook.xlsx.writeFile(Obj.op_file);
    
    for(let i=0,len=Obj.filelist.length;i<len;i++)
    {
        ;
        ((path.extname(Obj.filelist[i].filepath)).toLowerCase()==".csv")?await csvtoexcel({targetWorkbook,fileObj:Obj.filelist[i],op_file:Obj.op_file}):
         await xls2xlsx({targetWorkbook,fileObj:Obj.filelist[i],op_file:Obj.op_file});
    }
    return {"status":"success","msg":""}
}
