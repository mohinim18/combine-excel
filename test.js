const expect = require('chai').expect;
// import math file
const main = require('./index');

describe('Combine Excel', () => {
    it('Multi CSV to Excel', async() => {
        let params={
            "filelist": [
                {
                    "filepath": "./sheets/1.csv",
                    "sheet_name": "Transaction",
                    "header-bold":true
                },
                {
                    "filepath": "./sheets/2.csv",
                    "sheet_name": "Holding",
                    "header-bold":true
                }
            ],
            "op_file": "./sheets/CombinedCSV.xlsx"
        }
        const result =await main(params);
        console.log(result)
        expect(result.status).to.equal("success");
    });
    it('Multi XLSX to XLSX', async() => {
        let params={
            "filelist": [
                {
                    "filepath": "./sheets/Nav1.xlsx",
                    "sheet_name": "Transaction",
                    "header-bold":true
                },
                {
                    "filepath": "./sheets/NIPPON INDIA ETF Bank BeES-Norm.xlsx",
                    "sheet_name": "Holding",
                    "header-bold":true
                }
            ],
            "op_file": "./sheets/CombinedXLS.xlsx"
        }
        let result
        try {
            result =await main(params);    
            console.log(result)
        } catch (error) {
            console.log(error.stack)
            result={"status":"unsuccess"}
        }
        console.log(result);
        expect(result.status).to.equal("success");
    });

});