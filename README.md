### Documentation
-   Convert multiple CSV files into a single excel with multiple sheet

### I/P
```json
{
    "filelist": [
        {
            "filepath": "./files/1.csv", //name of the csv file
            "sheet_name": "Portfolio",  // Name of the sheet in excel to be created 
            "header-bold":true          // First record to be bold 
        },
        {
            "filepath": "./files/2.csv",
            "sheet_name": "Transaction"
        }
    ],
    "op_file": "Combined.xlsx"  //O/P filename with path
}
```

### O/P
```json
{"status":"success"}    // In case successfully created else will throw an error
```

### Test
-   npm run test